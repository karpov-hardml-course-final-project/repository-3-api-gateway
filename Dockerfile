# syntax=docker/dockerfile:1
FROM clearlinux/numpy-mp:latest
WORKDIR /code

ENV FLASK_APP=app.py
ENV FLASK_RUN_HOST=0.0.0.0
ENV FLASK_RUN_PORT=8080

RUN swupd update && swupd bundle-add git
COPY requirements.txt requirements.txt

RUN pip install -r requirements.txt

EXPOSE 8080

COPY . .

ENTRYPOINT [ "python" ]

CMD ["app.py" ]