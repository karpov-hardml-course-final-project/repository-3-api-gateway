import os
import numpy as np
import json

from flask import Flask, request, jsonify, abort
import requests

from qatoolbox import PrecalcClusters

worker_address = os.environ.get("WORKER_ADDRESS")
embedder_address = os.environ.get("EMBEDDER_ADDRESS")
stage = os.environ.get("STAGE", "blue")
model_dir = os.environ.get("MODEL_DIR", f"/var/models/{stage}")

print(worker_address)
print(embedder_address)
print(stage)
print(model_dir)

app = Flask(__name__)

cluster_model = PrecalcClusters(os.path.join(model_dir, "clusters_centers_use.pkl"))


@app.route("/search", methods=['GET'])
def matching():
    """
    GET /search?q=<str>
    который возвращает {"matching": [str]}
    """
    args = request.args
    q = args.get('q', type=str)
    k = args.get('k', type=int, default=10)

    r = requests.post(embedder_address, json={"instances": [q]})
    if r.status_code != 200:
        return json.dumps({"status": "embedder service return incorrect reponse"})
    
    embedding = r.json()['predictions'][0]
    cluster_label = cluster_model.predict_label(np.array(embedding))
    r = requests.post(f"{worker_address}/search", json={"query": embedding, "cluster": cluster_label, "k": k})
    if r.status_code != 200:
        return json.dumps({"status": "faiss service return incorrect reponse"})
    else:
        return jsonify({"matching": r.json()["matching"]})


@app.route("/healthcheck")
def health():
    if cluster_model:
        return "ok"
    else:
        return abort(501)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)